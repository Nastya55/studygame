package baby.kid.studygame;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SecondScreen extends AppCompatActivity {

    private long timeReverseClick;
    private Toast toastReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_screen);

        @SuppressLint("WrongViewCast") ImageView btn_anim = (ImageView) findViewById(R.id.btn_anim);
        @SuppressLint("WrongViewCast") ImageView btn_coloring = (ImageView) findViewById(R.id.btn_coloring);
        @SuppressLint("WrongViewCast") ImageView btn_shapes = (ImageView) findViewById(R.id.btn_shapes);
        @SuppressLint("WrongViewCast") ImageView btn_sur_world = (ImageView) findViewById(R.id.btn_sur_world);
        btn_anim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(SecondScreen.this, MainAniLevels.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });

        btn_coloring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent1 = new Intent(SecondScreen.this, MainCollLevels.class);
                    startActivity(intent1);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });

        btn_shapes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(SecondScreen.this, MainShapLevels.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });

        btn_sur_world.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(SecondScreen.this, MainSurWorldLevels.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });


        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //кнопка "назад"
        Button btn_back = (Button)findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Здесь будет команда для кнопки

                //начало конструкции
                try {
                    Intent intent = new Intent(SecondScreen.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });

        //Подключаем анимацию - начало
        final Animation a = AnimationUtils.loadAnimation(SecondScreen.this, R.anim.alpha);
        btn_anim.startAnimation(a);
        btn_coloring.startAnimation(a);
        btn_shapes.startAnimation(a);
        btn_sur_world.startAnimation(a);
        //Подключаем анимацию - конец
    }


    //Системная кнопка "Назад" - начало
    @Override
    public void onBackPressed() {
        if (timeReverseClick + 2000 > System.currentTimeMillis()) {
            toastReturn.cancel();
            super.onBackPressed();
            return;
        } else {
            toastReturn = Toast.makeText(getBaseContext(), "Нажмите ещё раз, чтобы выйти", Toast.LENGTH_SHORT);
            toastReturn.show();
        }

        timeReverseClick = System.currentTimeMillis();
    }
    //Системная кнопка "Назад" - конец

}
