package baby.kid.studygame;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ShapLevelOne extends AppCompatActivity {

    private long timeReverseClick;
    private Toast toastReturn;

    Dialog dialogleft;
    Dialog dialogcenter;
    Dialog dialogright;


    public int numLeft;
    public int numCenter;
    public int numRight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shap_one);

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        final ImageView img_left1 = (ImageView)findViewById(R.id.img_left1);
        final ImageView img_left2 = (ImageView)findViewById(R.id.img_left2);
        final ImageView img_center = (ImageView)findViewById(R.id.img_center);
        final ImageView img_right1 = (ImageView)findViewById(R.id.img_right1);
        final ImageView img_right2 = (ImageView)findViewById(R.id.img_right2);
        final ImageView img_right3 = (ImageView)findViewById(R.id.img_right3);

        //кнопка "назад"
        Button btn_back = (Button)findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Здесь будет команда для кнопки

                //начало конструкции
                try {
                    Intent intent = new Intent(ShapLevelOne.this, MainShapLevels.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });


        //Подключаем анимацию - начало
        final Animation a = AnimationUtils.loadAnimation(ShapLevelOne.this, R.anim.alpha);
        //Подключаем анимацию - конец

        //Обрабатываем нажатие на первую левую картинку - начало
        img_left1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Условие касания картинки - начало
                if (event.getAction()==MotionEvent.ACTION_DOWN){
                    //если коснулся картинки - начало
                    img_left2.setEnabled(false);//блокируем 2 левую картинку
                    img_center.setEnabled(false);//блокируем среднюю картинку
                    img_right1.setEnabled(false);//блокируем 1 правую картинку
                    img_right2.setEnabled(false);//блокируем 2 правую картинку
                    img_right3.setEnabled(false);//блокируем 3 правую картинку
                    if (numLeft>numCenter){
                        img_left1.setImageResource(R.drawable.img_true);
                    }else {
                        img_left1.setImageResource(R.drawable.img_false);
                    }
                    //если коснулся картинки - конец
                }else if(event.getAction()==MotionEvent.ACTION_UP){
                    //если отпустил палец - начало
                    if (numLeft>numCenter){
                    }
                    //если отпустил палец - конец
                    dialogleft.show();//показать диалоговое окно
                }
                //Условие касания картинки - конец
                return true;
            }
        });

        //Обрабатываем нажатие на 2 левую картинку - начало
        img_left2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Условие касания картинки - начало
                if (event.getAction()==MotionEvent.ACTION_DOWN){
                    //если коснулся картинки - начало
                    img_left1.setEnabled(false);//блокируем 1 левую картинку
                    img_center.setEnabled(false);//блокируем среднюю картинку
                    img_right1.setEnabled(false);//блокируем 1 правую картинку
                    img_right2.setEnabled(false);//блокируем 2 правую картинку
                    img_right3.setEnabled(false);//блокируем 3 правую картинку
                    if (numLeft>numCenter){
                        img_left2.setImageResource(R.drawable.img_true);
                    }else {
                        img_left2.setImageResource(R.drawable.img_false);
                    }
                    //если коснулся картинки - конец
                }else if(event.getAction()==MotionEvent.ACTION_UP){
                    //если отпустил палец - начало
                    if (numLeft>numCenter){
                    }
                    //если отпустил палец - конец
                    dialogleft.show();//показать диалоговое окно
                }
                //Условие касания картинки - конец
                return true;
            }
        });
        //_______________________________________________________
        //Вызов диалогового окна в конце игры - начало
        dialogleft = new Dialog(this); //создаём новое диалоговое окно
        dialogleft.requestWindowFeature(Window.FEATURE_NO_TITLE); //скрываем заголовок
        dialogleft.setContentView(R.layout.dialog_wrong); //путь к макету диалогового окна
        dialogleft.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT)); //прозрачный фон диалогового окна
        dialogleft.setCancelable(false);//окно нельзя закрыть кнопкой "Назад(системной)"
        //кнопка, которая закрывает диалоговое окно - начало
        TextView btnclose1 = (TextView) dialogleft.findViewById(R.id.btnclose1);
        btnclose1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //обрабатывает нажатие кнопки - начало
                try {
                    //Вернуться назад к выбору уровня - начало
                    Intent intent = new Intent(ShapLevelOne.this, MainShapLevels.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору уровня - конец
                } catch (Exception e) {
                    //здесь кода не будет
                }
                dialogleft.dismiss();//закрываем диалоговое окно
                //обрабатывает нажатие кнопки - конец
            }
        });
        //кнопка, которая закрывает диалоговое окно - конец

        //кнопка "Продолжить" - начало
        Button btncontinue1 = (Button) dialogleft.findViewById(R.id.btncontinue1);
        btncontinue1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(ShapLevelOne.this, ShapLevelOne.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору уровня - конец
                } catch (Exception e) {
                    //здесь кода не будет

                }
                dialogleft.dismiss(); //закрываем диалоговое окно
            }
        });

        //кнопка "Продолжить" - конец
        //Вызов диалогового окна в конце игры - конец
        //_________________________________________________________

        //_______________________________________________________
        //Вызов диалогового окна в конце игры - начало
        dialogcenter = new Dialog(this); //создаём новое диалоговое окно
        dialogcenter.requestWindowFeature(Window.FEATURE_NO_TITLE); //скрываем заголовок
        dialogcenter.setContentView(R.layout.dialog_wrong); //путь к макету диалогового окна
        dialogcenter.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT)); //прозрачный фон диалогового окна
        dialogcenter.setCancelable(false);//окно нельзя закрыть кнопкой "Назад(системной)"
        //кнопка, которая закрывает диалоговое окно - начало
        TextView btnclose2 = (TextView) dialogcenter.findViewById(R.id.btnclose1);
        btnclose2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //обрабатывает нажатие кнопки - начало
                try {
                    //Вернуться назад к выбору уровня - начало
                    Intent intent = new Intent(ShapLevelOne.this, MainShapLevels.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору уровня - конец
                } catch (Exception e) {
                    //здесь кода не будет
                }
                dialogcenter.dismiss();//закрываем диалоговое окно
                //обрабатывает нажатие кнопки - конец
            }
        });
        //кнопка, которая закрывает диалоговое окно - конец

        //кнопка "Продолжить" - начало
        Button btncontinue2 = (Button) dialogcenter.findViewById(R.id.btncontinue1);
        btncontinue2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(ShapLevelOne.this, ShapLevelOne.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору уровня - конец
                } catch (Exception e) {
                    //здесь кода не будет

                }
                dialogcenter.dismiss(); //закрываем диалоговое окно
            }
        });

        //кнопка "Продолжить" - конец
        //Вызов диалогового окна в конце игры - конец
        //_________________________________________________________

        //_______________________________________________________
        //Вызов диалогового окна в конце игры - начало
        dialogright = new Dialog(this, R.style.dialogstyle); //создаём новое диалоговое окно
        dialogright.requestWindowFeature(Window.FEATURE_NO_TITLE); //скрываем заголовок
        dialogright.setContentView(R.layout.dialog_correctly); //путь к макету диалогового окна
        dialogright.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT)); //прозрачный фон диалогового окна
        dialogright.setCancelable(false);//окно нельзя закрыть кнопкой "Назад(системной)"
        //кнопка, которая закрывает диалоговое окно - начало
        TextView btnclosetrue = (TextView) dialogright.findViewById(R.id.btnclosetrue);
        btnclosetrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //обрабатывает нажатие кнопки - начало
                try {
                    //Вернуться назад к выбору уровня - начало
                    Intent intent = new Intent(ShapLevelOne.this, MainShapLevels.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору уровня - конец
                } catch (Exception e) {
                    //здесь кода не будет
                }
                dialogright.dismiss();//закрываем диалоговое окно
                //обрабатывает нажатие кнопки - конец
            }
        });
        //кнопка, которая закрывает диалоговое окно - конец

        //кнопка "Продолжить" - начало
        Button btncontinuetrue = (Button) dialogright.findViewById(R.id.btncontinuetrue);
        btncontinuetrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!MainShapLevels.isCompleated[0])
                    MainShapLevels.count++;

                MainShapLevels.isCompleated[0] = true;
                Intent intent = new Intent(ShapLevelOne.this, ShapLevelTwo.class); //создали намерение для перехода
                startActivity(intent);//старт намерения
                finish();//закрыть этот класс
                //Вернуться назад к выбору уровня - конец
                dialogright.dismiss(); //закрываем диалоговое окно
            }
        });

        //кнопка "Продолжить" - конец
        //Вызов диалогового окна в конце игры - конец
        //_________________________________________________________


        //Обрабатываем нажатие на левую картинку - конец


        //Обрабатываем нажатие на среднюю картинку - начало
        img_center.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Условие касания картинки - начало
                if (event.getAction()==MotionEvent.ACTION_DOWN){
                    //если коснулся картинки - начало
                    img_left1.setEnabled(false);//блокируем 1 левую картинку
                    img_left2.setEnabled(false);//блокируем 2 левую картинку
                    img_right1.setEnabled(false);//блокируем 1 правую картинку
                    img_right2.setEnabled(false);//блокируем 2 правую картинку
                    img_right3.setEnabled(false);//блокируем 3 правую картинку
                    if (numCenter>numRight){
                        img_center.setImageResource(R.drawable.img_true);
                    }else {
                        img_center.setImageResource(R.drawable.img_false);
                    }
                    //если коснулся картинки - конец
                }else if(event.getAction()==MotionEvent.ACTION_UP){
                    //если отпустил палец - начало
                    if (numCenter>numRight){
                    }
                    //если отпустил палец - конец
                    dialogcenter.show();//показать диалоговое окно

                }
                //Условие касания картинки - конец
                return true;
            }
        });

        //Обрабатываем нажатие на среднюю картинку - конец

        //Обрабатываем нажатие на левую(после средней картинки) картинку - начало
        img_right1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Условие касания картинки - начало
                if (event.getAction()==MotionEvent.ACTION_DOWN){
                    //если коснулся картинки - начало
                    img_left1.setEnabled(false);//блокируем 1 левую картинку
                    img_left2.setEnabled(false);//блокируем 2 левую картинку
                    img_center.setEnabled(false);//блокируем среднюю картинку
                    img_right2.setEnabled(false);//блокируем 2 правую картинку
                    img_right3.setEnabled(false);//блокируем 3 правую картинку
                    if (numCenter>numRight){
                        img_right1.setImageResource(R.drawable.img_true);
                    }else {
                        img_right1.setImageResource(R.drawable.img_false);
                    }
                    //если коснулся картинки - конец
                }else if(event.getAction()==MotionEvent.ACTION_UP){
                    //если отпустил палец - начало
                    if (numCenter>numRight){
                    }
                    //если отпустил палец - конец
                    dialogcenter.show();//показать диалоговое окно

                }
                //Условие касания картинки - конец
                return true;
            }
        });
        //Обрабатываем нажатие на левую(после средней картинки) - конец


        //Обрабатываем нажатие на правую картинку - начало
        img_right2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Условие касания картинки - начало
                if (event.getAction()==MotionEvent.ACTION_DOWN){
                    //если коснулся картинки - начало
                    img_left1.setEnabled(false);//блокируем 1 левую картинку
                    img_left2.setEnabled(false);//блокируем 2 левую картинку
                    img_center.setEnabled(false);//блокируем среднюю картинку
                    img_right1.setEnabled(false);//блокируем 1 правую картинку
                    img_right3.setEnabled(false);//блокируем 3 правую картинку
                    if (numRight==0){
                        img_right2.setImageResource(R.drawable.img_true);
                    }else {
                        img_right2.setImageResource(R.drawable.img_false);
                    }
                    //если коснулся картинки - конец
                }else if(event.getAction()==MotionEvent.ACTION_UP){
                    //если отпустил палец - начало
                    if (numRight>numLeft){
                    }
                    //если отпустил палец - конец
                    dialogright.show();//показать диалоговое окно
                }
                //Условие касания картинки - конец
                return true;
            }
        });
        //Обрабатываем нажатие на правую картинку - конец

        //Обрабатываем нажатие на правую(после правильной картинки) картинку - начало
        img_right3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Условие касания картинки - начало
                if (event.getAction()==MotionEvent.ACTION_DOWN){
                    //если коснулся картинки - начало
                    img_left1.setEnabled(false);//блокируем 1 левую картинку
                    img_left2.setEnabled(false);//блокируем 2 левую картинку
                    img_center.setEnabled(false);//блокируем среднюю картинку
                    img_right1.setEnabled(false);//блокируем 1 правую картинку
                    img_right2.setEnabled(false);//блокируем 2 правую картинку
                    if (numRight>numLeft){
                        img_right3.setImageResource(R.drawable.img_true);
                    }else {
                        img_right3.setImageResource(R.drawable.img_false);
                    }
                    //если коснулся картинки - конец
                }else if(event.getAction()==MotionEvent.ACTION_UP){
                    //если отпустил палец - начало
                    if (numRight>numLeft){
                    }
                    //если отпустил палец - конец
                    dialogcenter.show();//показать диалоговое окно

                }
                //Условие касания картинки - конец
                return true;
            }
        });
        //Обрабатываем нажатие на правую(после правильной картинки) - конец

    }
    private void soundPlayButton(MediaPlayer sound) {
        sound.start();
    }

    //Системная кнопка "Назад" - начало
    @Override
    public void onBackPressed() {
        if (timeReverseClick + 2000 > System.currentTimeMillis()) {
            toastReturn.cancel();
            super.onBackPressed();
            return;
        } else {
            toastReturn = Toast.makeText(getBaseContext(), "Нажмите ещё раз, чтобы выйти", Toast.LENGTH_SHORT);
            toastReturn.show();
        }

        timeReverseClick = System.currentTimeMillis();
    }
    //Системная кнопка "Назад" - конец
}

