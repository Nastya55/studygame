package baby.kid.studygame;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ColsLevelFive extends AppCompatActivity {

    private long timeReverseClick;
    private Toast toastReturn;

    Dialog dialogleft;
    Dialog dialogcenter;
    Dialog dialogright;


    public int numLeft;
    public int numCenter;
    public int numRight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cols_five);

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        final ImageView img_left = (ImageView)findViewById(R.id.img_left);
        final ImageView img_center = (ImageView)findViewById(R.id.img_center);
        final ImageView img_right = (ImageView)findViewById(R.id.img_right);

        //кнопка "назад"
        Button btn_back = (Button)findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Здесь будет команда для кнопки

                //начало конструкции
                try {
                    Intent intent = new Intent(ColsLevelFive.this, MainCollLevels.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });


        //Подключаем анимацию - начало
        final Animation a = AnimationUtils.loadAnimation(ColsLevelFive.this, R.anim.alpha);
        //Подключаем анимацию - конец

        //Обрабатываем нажатие на левую картинку - начало
        img_left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Условие касания картинки - начало
                if (event.getAction()==MotionEvent.ACTION_DOWN){
                    //если коснулся картинки - начало
                    img_center.setEnabled(false);//блокируем среднюю картинку
                    img_right.setEnabled(false);//блокируем правую картинку
                    if (numLeft>numCenter){
                        img_left.setImageResource(R.drawable.img_true);
                    }else {
                        img_left.setImageResource(R.drawable.img_false);
                    }
                    //если коснулся картинки - конец
                }else if(event.getAction()==MotionEvent.ACTION_UP){
                    //если отпустил палец - начало
                    if (numLeft>numCenter){
                    }
                    //если отпустил палец - конец
                    dialogleft.show();//показать диалоговое окно
                }
                //Условие касания картинки - конец
                return true;
            }
        });
        //_______________________________________________________
        //Вызов диалогового окна в конце игры - начало
        dialogleft = new Dialog(this); //создаём новое диалоговое окно
        dialogleft.requestWindowFeature(Window.FEATURE_NO_TITLE); //скрываем заголовок
        dialogleft.setContentView(R.layout.dialog_wrong); //путь к макету диалогового окна
        dialogleft.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT)); //прозрачный фон диалогового окна
        dialogleft.setCancelable(false);//окно нельзя закрыть кнопкой "Назад(системной)"
        //кнопка, которая закрывает диалоговое окно - начало
        TextView btnclose1 = (TextView) dialogleft.findViewById(R.id.btnclose1);
        btnclose1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //обрабатывает нажатие кнопки - начало
                try {
                    //Вернуться назад к выбору уровня - начало
                    Intent intent = new Intent(ColsLevelFive.this, MainCollLevels.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору уровня - конец
                } catch (Exception e) {
                    //здесь кода не будет
                }
                dialogleft.dismiss();//закрываем диалоговое окно
                //обрабатывает нажатие кнопки - конец
            }
        });
        //кнопка, которая закрывает диалоговое окно - конец

        //кнопка "Продолжить" - начало
        Button btncontinue1 = (Button) dialogleft.findViewById(R.id.btncontinue1);
        btncontinue1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(ColsLevelFive.this, ColsLevelFive.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору уровня - конец
                } catch (Exception e) {
                    //здесь кода не будет

                }
                dialogleft.dismiss(); //закрываем диалоговое окно
            }
        });

        //кнопка "Продолжить" - конец
        //Вызов диалогового окна в конце игры - конец
        //_________________________________________________________

        //_______________________________________________________
        //Вызов диалогового окна в конце игры - начало
        dialogcenter = new Dialog(this, R.style.dialogstyle); //создаём новое диалоговое окно
        dialogcenter.requestWindowFeature(Window.FEATURE_NO_TITLE); //скрываем заголовок
        dialogcenter.setContentView(R.layout.dialog_correctly); //путь к макету диалогового окна
        dialogcenter.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT)); //прозрачный фон диалогового окна
        dialogcenter.setCancelable(false);//окно нельзя закрыть кнопкой "Назад(системной)"
        //кнопка, которая закрывает диалоговое окно - начало
        TextView btnclosetrue = (TextView) dialogcenter.findViewById(R.id.btnclosetrue);
        btnclosetrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //обрабатывает нажатие кнопки - начало
                try {
                    //Вернуться назад к выбору уровня - начало
                    Intent intent = new Intent(ColsLevelFive.this, MainCollLevels.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору уровня - конец
                } catch (Exception e) {
                    //здесь кода не будет
                }
                dialogcenter.dismiss();//закрываем диалоговое окно
                //обрабатывает нажатие кнопки - конец
            }
        });
        //кнопка, которая закрывает диалоговое окно - конец

        //кнопка "Продолжить" - начало
        Button btncontinuetrue = (Button) dialogcenter.findViewById(R.id.btncontinuetrue);
        btncontinuetrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (! MainCollLevels.isCompleated[4])
                    MainCollLevels.count++;

                MainCollLevels.isCompleated[4] = true;
                Intent intent = new Intent(ColsLevelFive.this, ColsLevelSix.class); //создали намерение для перехода
                startActivity(intent);//старт намерения
                finish();//закрыть этот класс
                //Вернуться назад к выбору уровня - конец

                dialogleft.dismiss(); //закрываем диалоговое окно
            }
        });

        //кнопка "Продолжить" - конец
        //Вызов диалогового окна в конце игры - конец
        //_________________________________________________________

        //_______________________________________________________
        //Вызов диалогового окна в конце игры - начало
        dialogright = new Dialog(this); //создаём новое диалоговое окно
        dialogright.requestWindowFeature(Window.FEATURE_NO_TITLE); //скрываем заголовок
        dialogright.setContentView(R.layout.dialog_wrong); //путь к макету диалогового окна
        dialogright.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT)); //прозрачный фон диалогового окна
        dialogright.setCancelable(false);//окно нельзя закрыть кнопкой "Назад(системной)"
        //кнопка, которая закрывает диалоговое окно - начало
        TextView btnclose3 = (TextView) dialogright.findViewById(R.id.btnclose1);
        btnclose3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //обрабатывает нажатие кнопки - начало
                try {
                    //Вернуться назад к выбору уровня - начало
                    Intent intent = new Intent(ColsLevelFive.this, MainCollLevels.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору уровня - конец
                } catch (Exception e) {
                    //здесь кода не будет
                }
                dialogright.dismiss();//закрываем диалоговое окно
                //обрабатывает нажатие кнопки - конец
            }
        });
        //кнопка, которая закрывает диалоговое окно - конец

        //кнопка "Продолжить" - начало
        Button btncontinue3 = (Button) dialogright.findViewById(R.id.btncontinue1);
        btncontinue3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(ColsLevelFive.this, ColsLevelFive.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору уровня - конец
                } catch (Exception e) {
                    //здесь кода не будет

                }
                dialogright.dismiss(); //закрываем диалоговое окно
            }
        });

        //кнопка "Продолжить" - конец
        //Вызов диалогового окна в конце игры - конец
        //_________________________________________________________
        //Обрабатываем нажатие на левую картинку - конец


        //Обрабатываем нажатие на среднюю картинку - начало
        img_center.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Условие касания картинки - начало
                if (event.getAction()==MotionEvent.ACTION_DOWN){
                    //если коснулся картинки - начало
                    img_left.setEnabled(false);//блокируем среднюю картинку
                    img_right.setEnabled(false);//блокируем правую картинку
                    if (numCenter==0){
                        img_center.setImageResource(R.drawable.img_true);
                    }else {
                        img_center.setImageResource(R.drawable.img_false);
                    }
                    //если коснулся картинки - конец
                }else if(event.getAction()==MotionEvent.ACTION_UP){
                    //если отпустил палец - начало
                    if (numCenter>numLeft){
                    }
                    //если отпустил палец - конец
                    dialogcenter.show();//показать диалоговое окно

                }
                //Условие касания картинки - конец
                return true;
            }
        });

        //Обрабатываем нажатие на среднюю картинку - конец

        //Обрабатываем нажатие на правую картинку - начало
        img_right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Условие касания картинки - начало
                if (event.getAction()==MotionEvent.ACTION_DOWN){
                    //если коснулся картинки - начало
                    img_left.setEnabled(false);//блокируем среднюю картинку
                    img_center.setEnabled(false);//блокируем правую картинку
                    if (numRight>numLeft){
                        img_right.setImageResource(R.drawable.img_true);
                    }else {
                        img_right.setImageResource(R.drawable.img_false);
                    }
                    //если коснулся картинки - конец
                }else if(event.getAction()==MotionEvent.ACTION_UP){
                    //если отпустил палец - начало
                    if (numRight>numLeft){
                    }
                    //если отпустил палец - конец
                    dialogright.show();//показать диалоговое окно
                }
                //Условие касания картинки - конец
                return true;
            }
        });

        //Обрабатываем нажатие на правую картинку - конец


    }

    private void soundPlayButton(MediaPlayer sound) {
        sound.start();
    }

    //Системная кнопка "Назад" - начало
    @Override
    public void onBackPressed() {
        if (timeReverseClick + 2000 > System.currentTimeMillis()) {
            toastReturn.cancel();
            super.onBackPressed();
            return;
        } else {
            toastReturn = Toast.makeText(getBaseContext(), "Нажмите ещё раз, чтобы выйти", Toast.LENGTH_SHORT);
            toastReturn.show();
        }

        timeReverseClick = System.currentTimeMillis();
    }
    //Системная кнопка "Назад" - конец
}
