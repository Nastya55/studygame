package baby.kid.studygame;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainAniLevels extends AppCompatActivity {

    private long timeReverseClick;
    private Toast toastReturn;
    public static int count = 0;

    public static boolean[] isCompleated = new boolean[20];

    final int[] levels = {
                R.id.alevel1, R.id.alevel2, R.id.alevel3, R.id.alevel4, R.id.alevel5,
                R.id.alevel6,R.id.alevel7,R.id.alevel8,R.id.alevel9,R.id.alevel10,
                R.id.alevel11, R.id.alevel12, R.id.alevel13, R.id.alevel14, R.id.alevel15,
                R.id.alevel16,R.id.alevel17,R.id.alevel18,R.id.alevel19,R.id.alevel20,
        };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anim_level);

        for (int i = 0; i < isCompleated.length; ++i) {
            if (isCompleated[i]) {
                TextView textView = findViewById(levels[i]);
                textView.setBackgroundResource(R.drawable.style_green);
            }
        }

        TextView countView = findViewById(R.id.textcount1);
        countView.setText(Integer.toString(count));

        final Dialog dialog;

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Вызов диалогового окна в начале игры - начало
        dialog = new Dialog(this); //создаём новое диалоговое окно
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //скрываем заголовок
        dialog.setContentView(R.layout.animalsdialog); //путь к макету диалогового окна
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT)); //прозрачный фон диалогового окна
        dialog.setCancelable(false);//окно нельзя закрыть кнопкой "Назад(системной)"
        //кнопка, которая закрывает диалоговое окно - начало
        TextView btnclose = (TextView) dialog.findViewById(R.id.btnclose);
        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //обрабатывает нажатие кнопки - начало
                try {
                    //Вернуться назад к выбору категории - начало
                    Intent intent = new Intent(MainAniLevels.this, MainAniLevels.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору категории - конец
                } catch (Exception e) {
                    //здесь кода не будет
                }
                dialog.dismiss();//закрываем диалоговое окно
                //обрабатывает нажатие кнопки - конец
            }
        });
        //кнопка, которая закрывает диалоговое окно - конец

        //кнопка "Продолжить" - начало
        Button btncontinue = (Button) dialog.findViewById(R.id.btncontinue);
        btncontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss(); //закрываем диалоговое окно
            }
        });

        //кнопка "Продолжить" - конец

        dialog.show();//показать диалоговое окно

        //Вызов диалогового окна в начале игры - конец


        //уровни - начало
        TextView alevel1 = (TextView) findViewById(R.id.alevel1);
        alevel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelOne.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel2 = (TextView) findViewById(R.id.alevel2);
        alevel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelTwo.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel3 = (TextView) findViewById(R.id.alevel3);
        alevel3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelThree.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel4 = (TextView) findViewById(R.id.alevel4);
        alevel4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelFour.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel5 = (TextView) findViewById(R.id.alevel5);
        alevel5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelFive.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel6 = (TextView) findViewById(R.id.alevel6);
        alevel6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelSix.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel7 = (TextView) findViewById(R.id.alevel7);
        alevel7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelSeven.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel8 = (TextView) findViewById(R.id.alevel8);
        alevel8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelEight.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel9 = (TextView) findViewById(R.id.alevel9);
        alevel9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelNine.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel10 = (TextView) findViewById(R.id.alevel10);
        alevel10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelTen.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel11 = (TextView) findViewById(R.id.alevel11);
        alevel11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelEleven.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel12 = (TextView) findViewById(R.id.alevel12);
        alevel12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelTwelve.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel13 = (TextView) findViewById(R.id.alevel13);
        alevel13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelThirteen.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel14 = (TextView) findViewById(R.id.alevel14);
        alevel14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelFourteen.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel15 = (TextView) findViewById(R.id.alevel15);
        alevel15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelFifteen.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel16 = (TextView) findViewById(R.id.alevel16);
        alevel16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelSixteen.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel17 = (TextView) findViewById(R.id.alevel17);
        alevel17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelSeventeen.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel18 = (TextView) findViewById(R.id.alevel18);
        alevel18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelEighteen.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel19 = (TextView) findViewById(R.id.alevel19);
        alevel19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelNineteen.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                } //конец конструкции
            }
        });
        TextView alevel20 = (TextView) findViewById(R.id.alevel20);
        alevel20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainAniLevels.this, AnimLevelTwenty.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {

                } //конец конструкции
            }
        });

        //уровни - конец


        //кнопка "назад"
        Button btn_back = (Button) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Здесь будет команда для кнопки

                //начало конструкции
                try {
                    Intent intent = new Intent(MainAniLevels.this, SecondScreen.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {

                } //конец конструкции
            }
        });

    }

//    @Override
//    public void onSaveInstanceState(Bundle savedInstanceState) {
//        super.onSaveInstanceState(savedInstanceState);
//        savedInstanceState.putInt("count", count);
//    }
//
//    @Override
//    public void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//
//        count = savedInstanceState.getInt("count");
//        TextView countView = findViewById(R.id.textcount1);
//        System.out.println(count);
//        System.out.println("Denis loh");
//        countView.setText("Denis govno");
//        countView.invalidate();
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        System.out.println("COUNT: " + count);
//    }

    ///Системная кнопка "Назад" - начало
    @Override
    public void onBackPressed() {
        if (timeReverseClick + 2000 > System.currentTimeMillis()) {
            toastReturn.cancel();
            super.onBackPressed();
            return;
        } else {
            toastReturn = Toast.makeText(getBaseContext(), "Нажмите ещё раз, чтобы выйти", Toast.LENGTH_SHORT);
            toastReturn.show();
        }

        timeReverseClick = System.currentTimeMillis();
    }
    //Системная кнопка "Назад" - конец

}
