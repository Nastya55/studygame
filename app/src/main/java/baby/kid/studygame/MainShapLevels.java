package baby.kid.studygame;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainShapLevels extends AppCompatActivity {

    private long timeReverseClick;
    private Toast toastReturn;
    public static int count = 0;

    public static boolean[] isCompleated = new boolean[15];

    final int[] levels = {
            R.id.slevel1, R.id.slevel2, R.id.slevel3, R.id.slevel4, R.id.slevel5,
            R.id.slevel6,R.id.slevel7,R.id.slevel8,R.id.slevel9,R.id.slevel10,
            R.id.slevel11,R.id.slevel12,R.id.slevel13,R.id.slevel14,R.id.slevel15,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shap_level);

        for (int i = 0; i < isCompleated.length; ++i) {
            if (isCompleated[i]) {
                TextView textView = findViewById(levels[i]);
                textView.setBackgroundResource(R.drawable.style_green);
            }
        }

        TextView countView = findViewById(R.id.textcount1);
        countView.setText(Integer.toString(count));

        final Dialog dialog;

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Вызов диалогового окна в начале игры - начало
        dialog = new Dialog(this); //создаём новое диалоговое окно
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //скрываем заголовок
        dialog.setContentView(R.layout.shapdialog); //путь к макету диалогового окна
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT)); //прозрачный фон диалогового окна
        dialog.setCancelable(false);//окно нельзя закрыть кнопкой "Назад(системной)"

        //кнопка, которая закрывает диалоговое окно - начало
        TextView btnclose = (TextView)dialog.findViewById(R.id.btnclose);
        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //обрабатывает нажатие кнопки - начало
                try {
                    //Вернуться назад к выбору категории - начало
                    Intent intent = new Intent(MainShapLevels.this, MainShapLevels.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору категории - конец
                } catch (Exception e) {
                    //здесь кода не будет
                }
                dialog.dismiss();//закрываем диалоговое окно
                //обрабатывает нажатие кнопки - конец
            }
        });
        //кнопка, которая закрывает диалоговое окно - конец

        //кнопка "Продолжить" - начало
        Button btncontinue = (Button)dialog.findViewById(R.id.btncontinue);
        btncontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss(); //закрываем диалоговое окно
            }
        });

        //кнопка "Продолжить" - конец

        dialog.show();//показать диалоговое окно

        //Вызов диалогового окна в начале игры - конец

        //уровни
        TextView slevel1 = (TextView)findViewById(R.id.slevel1);
        slevel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelOne.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel2 = (TextView)findViewById(R.id.slevel2);
        slevel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelTwo.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel3 = (TextView)findViewById(R.id.slevel3);
        slevel3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelThree.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel4 = (TextView)findViewById(R.id.slevel4);
        slevel4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelFour.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel5 = (TextView)findViewById(R.id.slevel5);
        slevel5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelFive.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel6 = (TextView)findViewById(R.id.slevel6);
        slevel6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelSix.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel7 = (TextView)findViewById(R.id.slevel7);
        slevel7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelSeven.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel8 = (TextView)findViewById(R.id.slevel8);
        slevel8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelEight.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel9 = (TextView)findViewById(R.id.slevel9);
        slevel9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelNine.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel10 = (TextView)findViewById(R.id.slevel10);
        slevel10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelTen.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel11 = (TextView)findViewById(R.id.slevel11);
        slevel11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelEleven.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel12 = (TextView)findViewById(R.id.slevel12);
        slevel12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelTwelve.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel13 = (TextView)findViewById(R.id.slevel13);
        slevel13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelThirteen.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel14 = (TextView)findViewById(R.id.slevel14);
        slevel14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelFourteen.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView slevel15 = (TextView)findViewById(R.id.slevel15);
        slevel15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainShapLevels.this, ShapLevelFifteen.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });

        //кнопка "назад"
       Button btn_back = (Button)findViewById(R.id.btn_back);
       btn_back.setOnClickListener(new View.OnClickListener() {
           @Override
            public void onClick(View v) {
               //Здесь будет команда для кнопки

                //начало конструкции
                try {
                    Intent intent = new Intent(MainShapLevels.this, SecondScreen.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
    }

    //Системная кнопка "Назад" - начало
    @Override
    public void onBackPressed() {
        if (timeReverseClick + 2000 > System.currentTimeMillis()) {
            toastReturn.cancel();
            super.onBackPressed();
            return;
        } else {
            toastReturn = Toast.makeText(getBaseContext(), "Нажмите ещё раз, чтобы выйти", Toast.LENGTH_SHORT);
            toastReturn.show();
        }

        timeReverseClick = System.currentTimeMillis();
    }
    //Системная кнопка "Назад" - конец

}
