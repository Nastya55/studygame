package baby.kid.studygame;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainCollLevels extends AppCompatActivity {

    private long timeReverseClick;
    private Toast toastReturn;
    public static int count = 0;

    public static boolean[] isCompleated = new boolean[10];

    final int[] levels = {
            R.id.clevel1, R.id.clevel2, R.id.clevel3, R.id.clevel4, R.id.clevel5,
            R.id.clevel6,R.id.clevel7,R.id.clevel8,R.id.clevel9,R.id.clevel10,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coll_level);

        for (int i = 0; i < isCompleated.length; ++i) {
            if (isCompleated[i]) {
                TextView textView = findViewById(levels[i]);
                textView.setBackgroundResource(R.drawable.style_green);
            }
        }

        TextView countView = findViewById(R.id.textcount1);
        countView.setText(Integer.toString(count));

        final Dialog dialog;

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Вызов диалогового окна в начале игры - начало
        dialog = new Dialog(this); //создаём новое диалоговое окно
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //скрываем заголовок
        dialog.setContentView(R.layout.colsdialog); //путь к макету диалогового окна
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT)); //прозрачный фон диалогового окна
        dialog.setCancelable(false);//окно нельзя закрыть кнопкой "Назад(системной)"

        //кнопка, которая закрывает диалоговое окно - начало
        TextView btnclose = (TextView)dialog.findViewById(R.id.btnclose);
        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //обрабатывает нажатие кнопки - начало
                try {
                    //Вернуться назад к выбору категории - начало
                    Intent intent = new Intent(MainCollLevels.this, MainCollLevels.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору категории - конец
                } catch (Exception e) {
                    //здесь кода не будет
                }
                dialog.dismiss();//закрываем диалоговое окно
                //обрабатывает нажатие кнопки - конец
            }
        });
        //кнопка, которая закрывает диалоговое окно - конец

        //кнопка "Продолжить" - начало
        Button btncontinue = (Button)dialog.findViewById(R.id.btncontinue);
        btncontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss(); //закрываем диалоговое окно
            }
        });

        //кнопка "Продолжить" - конец

        dialog.show();//показать диалоговое окно

        //Вызов диалогового окна в начале игры - конец

        //уровни
        TextView clevel1 = (TextView)findViewById(R.id.clevel1);
        clevel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainCollLevels.this, ColsLevelOne.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView clevel2 = (TextView)findViewById(R.id.clevel2);
        clevel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainCollLevels.this, ColsLevelTwo.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView clevel3 = (TextView)findViewById(R.id.clevel3);
        clevel3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainCollLevels.this, ColsLevelThree.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView clevel4 = (TextView)findViewById(R.id.clevel4);
        clevel4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainCollLevels.this, ColsLevelFour.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView clevel5 = (TextView)findViewById(R.id.clevel5);
        clevel5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainCollLevels.this, ColsLevelFive.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView clevel6 = (TextView)findViewById(R.id.clevel6);
        clevel6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainCollLevels.this, ColsLevelSix.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView clevel7 = (TextView)findViewById(R.id.clevel7);
        clevel7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainCollLevels.this, ColsLevelSeven.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView clevel8 = (TextView)findViewById(R.id.clevel8);
        clevel8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainCollLevels.this, ColsLevelEight.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView clevel9 = (TextView)findViewById(R.id.clevel9);
        clevel9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainCollLevels.this, ColsLevelNine.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView clevel10 = (TextView)findViewById(R.id.clevel10);
        clevel10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainCollLevels.this, ColsLevelTen.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });


        //кнопка "назад"
        Button btn_back = (Button)findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Здесь будет команда для кнопки

                //начало конструкции
                try {
                    Intent intent = new Intent(MainCollLevels.this, SecondScreen.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
    }

    //Системная кнопка "Назад" - начало
    @Override
    public void onBackPressed() {
        if (timeReverseClick + 2000 > System.currentTimeMillis()) {
            toastReturn.cancel();
            super.onBackPressed();
            return;
        } else {
            toastReturn = Toast.makeText(getBaseContext(), "Нажмите ещё раз, чтобы выйти", Toast.LENGTH_SHORT);
            toastReturn.show();
        }

        timeReverseClick = System.currentTimeMillis();
    }
    //Системная кнопка "Назад" - конец

}
