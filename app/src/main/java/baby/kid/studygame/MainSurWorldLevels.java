package baby.kid.studygame;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainSurWorldLevels extends AppCompatActivity {

    private long timeReverseClick;
    private Toast toastReturn;
    public static int count = 0;

    public static boolean[] isCompleated = new boolean[15];

    final int[] levels = {
                R.id.wlevel1, R.id.wlevel2, R.id.wlevel3, R.id.wlevel4, R.id.wlevel5,
                R.id.wlevel6,R.id.wlevel7,R.id.wlevel8,R.id.wlevel9,R.id.wlevel10,
                R.id.wlevel11,R.id.wlevel12,R.id.wlevel13,R.id.wlevel14,R.id.wlevel15,
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surworld_level);

        for (int i = 0; i < isCompleated.length; ++i) {
            if (isCompleated[i]) {
                TextView textView = findViewById(levels[i]);
                textView.setBackgroundResource(R.drawable.style_green);
            }
        }

        TextView countView = findViewById(R.id.textcount1);
        countView.setText(Integer.toString(count));

        final Dialog dialog;

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Вызов диалогового окна в начале игры - начало
        dialog = new Dialog(this); //создаём новое диалоговое окно
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //скрываем заголовок
        dialog.setContentView(R.layout.surworlddialog); //путь к макету диалогового окна
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT)); //прозрачный фон диалогового окна
        dialog.setCancelable(false);//окно нельзя закрыть кнопкой "Назад(системной)"

        //кнопка, которая закрывает диалоговое окно - начало
        TextView btnclose = (TextView)dialog.findViewById(R.id.btnclose);
        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //обрабатывает нажатие кнопки - начало
                try {
                    //Вернуться назад к выбору категории - начало
                    Intent intent = new Intent(MainSurWorldLevels.this, MainSurWorldLevels.class); //создали намерение для перехода
                    startActivity(intent);//старт намерения
                    finish();//закрыть этот класс
                    //Вернуться назад к выбору категории - конец
                } catch (Exception e) {
                    //здесь кода не будет
                }
                dialog.dismiss();//закрываем диалоговое окно
                //обрабатывает нажатие кнопки - конец
            }
        });
        //кнопка, которая закрывает диалоговое окно - конец

        //кнопка "Продолжить" - начало
        Button btncontinue = (Button)dialog.findViewById(R.id.btncontinue);
        btncontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss(); //закрываем диалоговое окно
            }
        });

        //кнопка "Продолжить" - конец

        dialog.show();//показать диалоговое окно

        //Вызов диалогового окна в начале игры - конец

        //уровни
        TextView wlevel1 = (TextView)findViewById(R.id.wlevel1);
        wlevel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelOne.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel2 = (TextView)findViewById(R.id.wlevel2);
        wlevel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelTwo.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel3 = (TextView)findViewById(R.id.wlevel3);
        wlevel3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelThree.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel4 = (TextView)findViewById(R.id.wlevel4);
        wlevel4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelFour.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel5 = (TextView)findViewById(R.id.wlevel5);
        wlevel5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelFive.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel6 = (TextView)findViewById(R.id.wlevel6);
        wlevel6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelSix.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel7 = (TextView)findViewById(R.id.wlevel7);
        wlevel7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelSeven.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel8 = (TextView)findViewById(R.id.wlevel8);
        wlevel8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelEight.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel9 = (TextView)findViewById(R.id.wlevel9);
        wlevel9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelNine.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel10 = (TextView)findViewById(R.id.wlevel10);
        wlevel10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelTen.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel11 = (TextView)findViewById(R.id.wlevel11);
        wlevel11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelEleven.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel12 = (TextView)findViewById(R.id.wlevel12);
        wlevel12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelTwelve.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel13 = (TextView)findViewById(R.id.wlevel13);
        wlevel13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelThirteen.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel14 = (TextView)findViewById(R.id.wlevel14);
        wlevel14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelFourteen.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
        TextView wlevel15 = (TextView)findViewById(R.id.wlevel15);
        wlevel15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SurWorldLevelFifteen.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });



        //кнопка "назад"
        Button btn_back = (Button)findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Здесь будет команда для кнопки

                //начало конструкции
                try {
                    Intent intent = new Intent(MainSurWorldLevels.this, SecondScreen.class);
                    startActivity(intent);
                    finish();
                }catch (Exception e){

                } //конец конструкции
            }
        });
    }

    //Системная кнопка "Назад" - начало
    @Override
    public void onBackPressed() {
        if (timeReverseClick + 2000 > System.currentTimeMillis()) {
            toastReturn.cancel();
            super.onBackPressed();
            return;
        } else {
            toastReturn = Toast.makeText(getBaseContext(), "Нажмите ещё раз, чтобы выйти", Toast.LENGTH_SHORT);
            toastReturn.show();
        }

        timeReverseClick = System.currentTimeMillis();
    }
    //Системная кнопка "Назад" - конец

}
