package baby.kid.studygame;

public class Counter {
    private int count;

    public Counter() {
        count = 0;
    }

    public int getCount() {
        return count;
    }

    public void add() {
        ++count;
    }
}
